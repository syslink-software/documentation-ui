;(function () {
  'use strict'

  var queryString = window.location.search
  var urlParams = new URLSearchParams(queryString)
  if (!urlParams.has('raw')) return

  ['toolbar', 'nav-container', 'toc', 'header', 'footer']
    .forEach((c) => {
      const items = [...document.getElementsByClassName(c)]
      items.forEach((item) => item.remove())
    })

  var links = [].filter.call(document.getElementsByTagName('a'), (el) => el.className.indexOf('page') >= 0)
  links.forEach((link) => {
    var url = new URL(link.href)
    url.setAttribute('search', 'raw')
    // url.search = 'raw'
    link.href = url
  })
})()
